# Portafolio -- Justo Andrés Manrique Urbina

Hola! Este proyecto tiene como finalidad demostrar mis habilidades dentro del ámbito de ciencia de datos. Para ello, he dividido este proyecto en 3 carpetas:
1) Visualización de datos
2) Proyectos de Kaggle finalizados
3) Creación de reportes automatizados

Cualquier consulta que derive de la exploración de dichas carpetas pueden derivarlas a mi correo personal: ja.manrique@pm.me